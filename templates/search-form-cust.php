<!-- Si le formulaire a un champ nommé s il redirigera vers la page de recherche par défaut. -->
<!-- Si l'intention n'est pas d'utiliser la page par défaut pour la recherche vous devrez ajouter certains trucs. -->
<!-- <form role="search" method="get" id="searchform" action="">
    <input type="hidden" name="action" value="werk_handle_form"> -->
<form role="search" method="post" id="searchform" action="<?php echo admin_url('admin-post.php') ?>">
    <input type="hidden" name="action" value="werk_handle_form">
    <?php echo wp_nonce_field('form-submit', '_nonce'); ?>
    <label for="s">Recherche pour:</label>
    <input type="text"  name="s" id="s" />
    <input type="hidden" value="1" name="sentence" />
    <input type="hidden" value="kraft_tasks" name="post_type" />
    <input type="submit" id="searchsubmit" value="Search" />
</form>
