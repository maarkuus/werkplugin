<?php
namespace Werk;
use Werk\Init;

class Activator
{
    function werk_init() { 
        new Init();
        flush_rewrite_rules();
    }
}
