<?php
require __DIR__ . '/vendor/autoload.php';
/**
 * Plugin Name:       Werk
 * Plugin URI:        dev.kraftwerk.local 
 * Description:       C'est un plugin
 * Version:           1.0.0
 * Author:            JP Lambert
 * Author URI:        dev.kraftwerk.local 
 * License:           GPL-2.0+
 * Text Domain:       werk
 */
use Werk\Activator;
$activator = new Activator();

$activator->werk_init();